<?php
namespace App\Http\DataProviders\Modules\Constant;

/**
 * Провайдер для Констант Operators
 *
 * @package 		DataProvider
 * @subpackage 		ConstantProvider
 * @author 			artem 
 * @version 		v.1.0 (07/06/2021)
 * @copyright 		Copyright (c) biz-on
 */
interface Operators
{
	const LIKE 			= 1;
	const NOT_LIKE 		= 2;
	const EQUAL 		= 3;
	const NOT_EQUAL 	= 4;
	const GT 			= 5;
	const GTE 			= 6;
	const LT 			= 7;
	const LTE 			= 8;
	const IN 			= 9;
	const NOT_IN 		= 10;
	const IS_NULL 		= 11;
	const IS_NOT_NULL 	= 12;
}