<?php
namespace App\Http\DataProviders\Modules\Item\Search\Primitives;

use App\Http\DataProviders\Modules\Item\Search\Primitives\PrimitiveInterface as PrimitiveInterface;


use App\Http\DataProviders\Modules\Constant\Operators as ConstantOperators;
use App\Http\DataProviders\Modules\Constant\ElementTypes as ConstantElementTypes;


/**
 *
 *
 * @package	 		DataProvider
 * @subpackage  	ItemProvider
 * @author	  		artem
 * @version	 		v.1.0 (07/06/2021)
 */
abstract class PrimitivesAbstract implements PrimitiveInterface, ConstantOperators, ConstantElementTypes
{

	/**
	 * разделитель для массива, переданного строкой
	 * @var string
	 */
	const ARRAY_DELIMITER = ',';

	protected $items_model;

	protected $filter;
	protected $conditional;
	protected $value;


	/**
	 * Основной метод взаимодействия. На вход - фильтры, на выход - запрос
	 * 
	 * @author 		artem
	 */
	abstract public function query_builder(array $filter);


	/**
	 * Получение строкового представления оператора, исходя из operator_id и контекста
	 * 
	 * @author 		artem
	 */
	abstract protected function get_operator(int $operator_id);


	/**
	 * Получение метода поиска, исходя из оператора и контекста
	 * 
	 * @author 		artem
	 */
	abstract protected function get_method(int $operator_id);


	/**
	 * Модификация значения исходя из контекста и оператора
	 * 
	 * приведение к массиву, вставка доп строк для like и другое
	 * @author 		artem
	 */
	abstract protected function modify_value($value, $conditional, $filter);


	/**
	 * value исходя из conditional и фильтра
	 * 
	 * @author 		artem
	 */
	abstract protected function normalize_value($value);


	/**
	 * Основной конструктор запросов
	 * 
	 * @author 		artem 
	 */
	abstract protected function build();


	/**
	 * Получение условий. Точка входа с выбором исходя из контекста
	 * 
	 * @author 		artem
	 */
	abstract protected function get_conditional(int $element_type_id, int $operator_id, $value);
}