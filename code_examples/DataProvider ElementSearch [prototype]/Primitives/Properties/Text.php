<?php
namespace App\Http\DataProviders\Modules\Item\Search\Primitives\Properties;

use App\Http\DataProviders\Modules\Item\Search\Primitives\PrimitivesAbstract as PrimitivesAbstract;

use DB;
use Exception;


/**
 * Поиск по Properties с предопределенными списками
 *
 * @package	 		DataProvider
 * @subpackage  	ItemProvider
 * @author	  		artem 
 * @version	 		v.1.0 (11/06/2021)
 * 
 */
class Text extends PrimitivesAbstract
{
	protected $items_model;

	protected $filter;
	protected $conditional;
	protected $value;
	
	public function __construct($items_model)
	{
		$this->items_model = $items_model;
	}


	/**
	 * билдер фильтров. основная точка входа
	 * 
	 * @author 		artem 
	 * @version	 	v.1.1 (11/06/2021)
	 * 4
	 */
	public function query_builder(array $filter)
	{
		$this->filter = $filter;
		$this->conditional = $this->get_conditional($filter ['object'] ['element_type_id'], 
												$filter ['operator_id'], 
												$filter ['value']);
		
		$this->value = $this->modify_value($filter ['value'], 
										$this->conditional,
										$this->filter);
		
		$this->build();
	}


	/**
	 * Основной конструктор запросов
	 * 
	 * @author 		artem 
	 * @version	 	v.1.1 (11/06/2021)
	 */
	protected function build()
	{
		$filter 		= $this->filter;
		$conditional 	= $this->conditional;
		$value 			= $this->value;

		switch ($conditional['method'])
		{
			case 'where':
				$this->items_model->whereIn('item_id', function ($sub_query) use ($filter, $conditional, $value)
				{
					$sub_query->select('item_id')
							->from('item_properties_value')
								->where('property_id', $filter ['object'] ['element_id'])
								->where('property_value', 	$conditional['operator'], $value)
							->get();
				});
			break;

			case 'whereIn':
				$this->items_model->whereIn('item_id', function ($sub_query) use ($filter, $conditional, $value)
				{
					$sub_query->select('item_id')
							->from('item_properties_value')
								->where('property_id', $filter ['object'] ['element_id'])
								->whereIn('property_value', 	$value)
							->get();
				});
			break;

			case 'whereNotIn':
				$this->items_model->whereIn('item_id', function ($sub_query) use ($filter, $conditional, $value)
				{
					$sub_query->select('item_id')
							->from('item_properties_value')
								->where('property_id', $filter ['object'] ['element_id'])
								->whereNotIn('property_value', 	$value)
							->get();
				});
			break;

			case 'whereNull':
				$this->items_model->whereNotIn('item_id', function ($sub_query) use ($filter, $conditional, $value)
				{
					$sub_query->select('item_id')
							->from('item_properties_value')
								->where('property_id', $filter ['object'] ['element_id'])
							->get();
				});
			break;

			case 'whereNotNull':
				$this->items_model->whereIn('item_id', function ($sub_query) use ($filter, $conditional, $value)
				{
					$sub_query->select('item_id')
							->from('item_properties_value')
								->where('property_id', $filter ['object'] ['element_id'])
							->get();
				});
			break;

			default:
				throw new Exception ('unsupported method: '. $conditional['method']);
		}
	}


	/**
	 * получение строкового представления оператора, исходя из operator_id и контекста
	 * 
	 * @author 		artem 
	 * @version	 	v.1.1 (11/06/2021)
	 */
	protected function get_operator(int $operator_id)
	{
		// todo валидация исходя из правил filter_element_type_operator_relation

		switch ($operator_id)
		{
			case self::LIKE:
				$operator = 'like';
			break;
			case self::NOT_LIKE:
				$operator = 'not like';
			break;
			case self::EQUAL:
				$operator = '=';
			break;
			case self::NOT_EQUAL:
				$operator = '<>';
			break;
			case self::GT:
				$operator = '>';
			break;
			case self::GTE:
				$operator = '>=';
			break;
			case self::LT:
				$operator = '<';
			break;
			case self::LTE:
				$operator = '<=';
			break;
			case self::IN:
				$operator = FALSE;
			break;
			case self::NOT_IN:
				$operator = FALSE;
			break;
			case self::IS_NULL:
				$operator = FALSE;
			break;
			case self::IS_NOT_NULL:
				$operator = FALSE;
			break;
			
			default:
				throw new Exception ('unsupported operator_id');
			break;
		}

		return $operator;
	}


	/**
	 * получение метода поиска, исходя из оператора и контекста
	 * 
	 * @author 		artem 
	 * @version	 	v.1.1 (11/06/2021)
	 * 4
	 */
	protected function get_method(int $operator_id)
	{
		switch ($operator_id)
		{
			case self::LIKE:
				$method = 'where';
			break;
			case self::NOT_LIKE:
				$method = 'where';
			break;
			case self::EQUAL:
				$method = 'where';
			break;
			case self::NOT_EQUAL:
				$method = 'where';
			break;
			case self::GT:
				$method = 'where';
			break;
			case self::GTE:
				$method = 'where';
			break;
			case self::LT:
				$method = 'where';
			break;
			case self::LTE:
				$method = 'where';
			break;
			case self::IN:
				$method = 'whereIn';
			break;
			case self::NOT_IN:
				$method = 'whereNotIn';
			break;
			case self::IS_NULL:
				$method = 'whereNull';
			break;
			case self::IS_NOT_NULL:
				$method = 'whereNotNull';
			break;
			
			default:
				throw new Exception ('unsupported operator_id');
		}

		return $method;
	}




	/**
	 * value исходя из conditional и фильтра
	 * 
	 * @author 		artem 
	 * @version	 	v.1.1 (11/06/2021)
	 */
	protected function modify_value($value, $conditional, $filter)
	{
		if (in_array($filter ['operator_id'], [self::IN, self::NOT_IN]))
		{
			$value = explode(self::ARRAY_DELIMITER, $value);
		}

		if (in_array($filter ['operator_id'], [self::LIKE, self::NOT_LIKE]))
		{
			$value = '%'. trim($value) .'%';
		}

		$value = $this->normalize_value($value);

		return $value;
	}


	/**
	 * value исходя из conditional и фильтра
	 * 
	 * @author 		artem 
	 * @version	 	v.1.1 (11/06/2021)
	 */
	protected function normalize_value($value)
	{
		if (is_array($value))
		{
			foreach ($value as $key => $sub_value)
			{
				if (is_string($sub_value))
				{
					$value [$key] = trim($value [$key]);
				}
			}
		}
		if (is_string($value))
		{
			$value = trim($value);
		}

		return $value;
	}


	/**
	 * -
	 * 
	 * @author 		artem 
	 * @version	 	v.1.1 (11/06/2021)
	 */
	protected function get_conditional(int $element_type_id, int $operator_id, $value): array
    {
		
		// подзапрос
		$column 	= FALSE;
		$operator 	= $this->get_operator($operator_id);
		$method 	= $this->get_method($operator_id);
	

		$response = [
			'column' 	=> $column,
			'method' 	=> $method,
			'operator' 	=> $operator,
			'value' 	=> $value,
		];
		

		return $response;
	}
}