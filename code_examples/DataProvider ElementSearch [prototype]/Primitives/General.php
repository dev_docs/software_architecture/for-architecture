<?php
namespace App\Http\DataProviders\Modules\Item\Search\Primitives;

use App\Http\DataProviders\Modules\Item\Search\Primitives\PrimitivesAbstract as PrimitivesAbstract;

use DB;
use Exception;



/**
 * Поиск по базовым фильтрам
 *
 * @author	  		artem
 */
class General extends PrimitivesAbstract
{
	protected $items_model;

	protected $filter;
	protected $conditional;
	protected $value;

	public function __construct($items_model)
	{
		$this->items_model = $items_model;
	}
	
	
	/**
	 * Билдер фильтров. Основная точка входа
	 *
	 * @throws Exception
	 * @author         artem
	 */
	public function query_builder(array $filter)
	{
		if (!isset($filter ['value']))
		{
			$filter ['value'] = NULL;
		}

		$this->filter = $filter;
		$this->conditional = $this->get_conditional($filter ['object'] ['element_type_id'], 
												$filter ['operator_id'], 
												$filter ['value']);
		
		$this->value = $this->modify_value($this->filter ['value'], 
										$this->conditional,
										$this->filter);
		
		$this->build();
	}
	
	
	/**
	 * Основной конструктор запросов
	 *
	 * @throws Exception
	 * @version        v.1.1 (03/06/2021)
	 * @todo           разобраться с method_exists
	 * @author         artem
	 */
	protected function build()
	{
		$conditional 	= $this->conditional;
		$value 			= $this->value;

		switch ($conditional['method'])
		{
			case 'where':
				$this->items_model->where($conditional['column'], $conditional['operator'], $value);
			break;
			case 'whereIn': 
				$this->items_model->whereIn($conditional['column'], $value);
			break;
			case 'whereNotIn':
				$this->items_model->whereNotIn($conditional['column'], $value);
			break;
			case 'whereNull':
				$this->items_model->whereNull($conditional['column']);
			break;
			case 'whereNotNull':
				$this->items_model->whereNotNull($conditional['column']);
			break;

			default:
				throw new Exception ('unsupported method: '. $conditional['method']);
		}
	}
	
	
	/**
	 * Получение строкового представления оператора, исходя из operator_id и контекста
	 *
	 * @param    int  $operator_id
	 * @return false|string
	 * @throws Exception
	 * @version        v.1.1 (03/06/2021)
	 * @author         artem
	 */
	protected function get_operator(int $operator_id)
	{
		// todo валидация исходя из правил filter_element_type_operator_relation

		switch ($operator_id)
		{
			case self::LIKE:
				$operator = 'like';
			break;
			case self::NOT_LIKE:
				$operator = 'not like';
			break;
			case self::EQUAL:
				$operator = '=';
			break;
			case self::NOT_EQUAL:
				$operator = '<>';
			break;
			case self::GT:
				$operator = '>';
			break;
			case self::GTE:
				$operator = '>=';
			break;
			case self::LT:
				$operator = '<';
			break;
			case self::LTE:
				$operator = '<=';
			break;
			case self::IN:
				$operator = FALSE;
			break;
			case self::NOT_IN:
				$operator = FALSE;
			break;
			case self::IS_NULL:
				$operator = FALSE;
			break;
			case self::IS_NOT_NULL:
				$operator = FALSE;
			break;
			
			default:
				throw new Exception ('unsupported operator_id');
		}

		return $operator;
	}
	
	
	/**
	 * Получение метода поиска, исходя из оператора и контекста
	 *
	 * @throws Exception
	 * @version        v.1.1 (03/06/2021)
	 * @author         artem
	 */
	protected function get_method(int $operator_id): string
	{
		$method;

		switch ($operator_id)
		{
			case self::LIKE:
				$method = 'where';
			break;
			case self::NOT_LIKE:
				$method = 'where';
			break;
			case self::EQUAL:
				$method = 'where';
			break;
			case self::NOT_EQUAL:
				$method = 'where';
			break;
			case self::GT:
				$method = 'where';
			break;
			case self::GTE:
				$method = 'where';
			break;
			case self::LT:
				$method = 'where';
			break;
			case self::LTE:
				$method = 'where';
			break;
			case self::IN:
				$method = 'whereIn';
			break;
			case self::NOT_IN:
				$method = 'whereNotIn';
			break;
			case self::IS_NULL:
				$method = 'whereNull';
			break;
			case self::IS_NOT_NULL:
				$method = 'whereNotNull';
			break;
			
			default:
				throw new Exception ('unsupported operator_id');
		}

		return $method;
	}
	
	
	/**
	 * value исходя из conditional и фильтра
	 *
	 * @param $value
	 * @param $conditional
	 * @param $filter
	 * @return mixed|string
	 * @author         artem
	 * @version        v.1.1 (04/06/2021)
	 */
	protected function modify_value($value, $conditional, $filter)
	{
		if (in_array($filter ['operator_id'], [self::IN, self::NOT_IN]))
		{
			if (!is_array($value))
			{
				$value = explode(self::ARRAY_DELIMITER, $value);
			}
		}

		if (in_array($filter ['operator_id'], [self::LIKE, self::NOT_LIKE]))
		{
			$value = '%'. trim($value) .'%';
		}

		$value = $this->normalize_value($value);

		return $value;
	}


	/**
	 * value исходя из conditional и фильтра
	 * 
	 * @author 		artem
	 * @version	 	v.1.1 (04/06/2021)
	 */
	protected function normalize_value($value)
	{
		if (is_array($value))
		{
			foreach ($value as $key => $sub_value)
			{
				if (is_string($sub_value))
				{
					$value [$key] = trim($sub_value);
				}
			}
		}
		if (is_string($value))
		{
			$value = trim($value);
		}

		return $value;
	}
	
	
	/**
	 * Получение условий. Точка входа с выбором исходя из контекста
	 *
	 * @param    int  $element_type_id
	 * @param    int  $operator_id
	 * @param         $value
	 * @return array
	 * @throws Exception
	 * @version        v.1.1 (03/06/2021)
	 * @author         artem
	 */
	protected function get_conditional(int $element_type_id, int $operator_id, $value): array
    {
		// todo можно переделать в матрицы
		switch ($element_type_id)
		{
			case self::CATEGORY:
				$column 	= 'category_id';
			break;

			case self::ITEM_TYPE:
				$column 	= 'item_type_id';
			break;

			case self::ITEM_STATUS:
				$column 	= 'status_id';
			break;
				
			case self::ITEM_ID:
				$column 	= 'item_id';
			break;
			
			case self::ITEM_NAME:
				$column 	= 'item_name';
			break;
				
				
			default:
				throw new Exception ('unsupported element_type_id');
		}

		$operator 	= $this->get_operator($operator_id);
		$method 	= $this->get_method($operator_id);

		$response = [
			'column' 	=> $column,
			'method' 	=> $method,
			'operator' 	=> $operator,
			'value' 	=> $value,
		];
		
		return $response;
	}
}