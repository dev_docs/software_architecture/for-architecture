<?php
namespace App\Http\DataProviders\Modules\Item\Search\Primitives;


use Exception;

use App\Http\DataProviders\Modules\Item\Properties as PropertiesModule;
use App\Http\DataProviders\Modules\Constant\Operators as ConstantOperators;
use App\Http\DataProviders\Modules\Constant\ElementTypes as ConstantElementTypes;


use App\Http\DataProviders\Modules\Item\Search\Primitives\Properties\Unit as PrimitivePropertiesUnit;
use App\Http\DataProviders\Modules\Item\Search\Primitives\Properties\Date as PrimitivePropertiesDate;
use App\Http\DataProviders\Modules\Item\Search\Primitives\Properties\Text as PrimitivePropertiesText;
use App\Http\DataProviders\Modules\Item\Search\Primitives\Properties\Select as PrimitivePropertiesSelect;
use App\Http\DataProviders\Modules\Item\Search\Primitives\Properties\Integer as PrimitivePropertiesInteger;

/**
 * Прокси стратегия
 *
 * @package	 		DataProvider
 * @subpackage  	ItemProvider
 * @author	  		artem
 * @version	 		v.1.0 (15/06/2021)
 */
class Properties implements ConstantOperators, ConstantElementTypes
{
	private $items_model 					= null;
	
	private $primitive_properties_unit 		= null;
	private $primitive_properties_text 		= null;
	private $primitive_properties_select 	= null;



	public function __construct($items_model)
	{
		// todo вынести в отдельный метод
		$this->items_model = $items_model;
		$this->primitive_properties_unit 	= new PrimitivePropertiesUnit($this->items_model);
		$this->primitive_properties_date 	= new PrimitivePropertiesDate($this->items_model);
		$this->primitive_properties_text 	= new PrimitivePropertiesText($this->items_model);
		$this->primitive_properties_select 	= new PrimitivePropertiesSelect($this->items_model);
		$this->primitive_properties_integer = new PrimitivePropertiesInteger($this->items_model);
	}
	
	
	/**
	 * билдер фильтров. основная точка входа
	 *
	 * @throws Exception
	 * @version        v.1.1 (15/06/2021)
	 * @author         artem
	 */
	public function query_builder(array $filter)
	{
		$property_type = PropertiesModule::get_property_type($filter ['object'] ['element_id']);

		if ($property_type == 'select')
		{
			$this->primitive_properties_select->query_builder($filter);
		}
		elseif($property_type == 'unit')
		{
			$this->primitive_properties_unit->query_builder($filter);
		}
		elseif($property_type == 'text' OR $property_type == 'rich_text')
		{
			$this->primitive_properties_text->query_builder($filter);
		}
		elseif($property_type == 'integer')
		{
			$this->primitive_properties_integer->query_builder($filter);
		}
		elseif($property_type == 'date')
		{
			$this->primitive_properties_date->query_builder($filter);
		}
		elseif($property_type == 'bool')
		{
			// temp
			$this->primitive_properties_integer->query_builder($filter);
		}
		elseif($property_type == 'money')
		{
			// temp
			$this->primitive_properties_text->query_builder($filter);
		}
		elseif($property_type == 'range')
		{
			// temp
			$this->primitive_properties_integer->query_builder($filter);
		}
		elseif($property_type == 'float')
		{
			// temp
			$this->primitive_properties_integer->query_builder($filter);
		}
		else
		{
			throw new Exception ('unsupproted property_type');
		}
	}
}