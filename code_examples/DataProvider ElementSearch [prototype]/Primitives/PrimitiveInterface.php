<?php
namespace App\Http\DataProviders\Modules\Item\Search\Primitives;

/**
 *
 *
 * @package	 		DataProvider
 * @subpackage  	ItemProvider
 * @author	  		artem
 * @version	 		v.1.0 (07/06/2021)
 */
interface PrimitiveInterface
{
	public function __construct($items_model);


	/**
	 * Основной метод взаимодействия. На вход - фильтры, на выход - запрос
	 * 
	 * @author 		artem
	 */
	public function query_builder(array $filter);
}