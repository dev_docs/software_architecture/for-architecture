<?php
namespace App\Http\DataProviders\Modules\Constant;

/**
 * Провайдер для Констант Operators
 *
 * @package 		DataProvider
 * @subpackage 		ConstantProvider
 * @author 			artem 
 * @version 		v.1.0 (07/06/2021)
 * @copyright 		Copyright (c) biz-on
 */
interface ElementTypes
{
	const ITEM 			= 1;
	const ITEM_TYPE 	= 2;
	const ITEM_STATUS 	= 3;
	const ITEM_PROPERTY = 4;
	const RELATION 		= 5;
	const CATEGORY 		= 6;
	const FILE 			= 7;
	const COMPANY 		= 8;
	const USER 			= 9;
	const PAYMENT 		= 10;
	const REMARK 		= 11;
	const SIGNATURE		= 12;
	const ITEM_CONFID	= 13;
	const ITEM_ID		= 14;
	const ITEM_NAME		= 15;
}