<?php
namespace App\Http\DataProviders\Modules\Item\Search;

use DB;
use Exception;
use Validator;



use App\Http\DataProviders\Modules\Constant\Operators as ConstantOperators;
use App\Http\DataProviders\Modules\Constant\ElementTypes as ConstantElementTypes;

use App\Http\DataProviders\Modules\Item\Search\Primitives\General as PrimitiveGeneral;
use App\Http\DataProviders\Modules\Item\Search\Primitives\Relation as PrimitiveRelation;
use App\Http\DataProviders\Modules\Item\Search\Primitives\Properties as PrimitiveProperties;


/**
 * Поиск по базовым фильтрам
 *
 * @package	 		DataProvider
 * @subpackage  	ItemProvider
 * @author	  		artem
 * @version	 		v.1.0
 */
class BaseFilters implements ConstantOperators, ConstantElementTypes
{

	private $response 				= null;
	private $filters 				= null;
	private $options 				= null;
	private $data 					= null;

	private $primitive_general 		= null;
	private $primitive_relation 	= null;
	private $primitive_properties 	= null;

	private $items_model 			= null;



	/**
	 * Конструктор класса
	 * 
	 * @author 		artem
	 * @version	 	v.1.0 (03/06/2021)
	 */
	function __construct ()
	{
		$this->items_model 					= DB::table('items')->where('is_trash', 0);
		
		$this->primitive_general 			= new PrimitiveGeneral($this->items_model);
		$this->primitive_relation 			= new PrimitiveRelation($this->items_model);
		$this->primitive_properties 		= new PrimitiveProperties($this->items_model);

		/* @todo
			- validate
			- limit/offset
			- tests (+mock)
			- other property types
			- properties module не оттестирован, код теоретический. там его ждет глубокая переработка, elementProps переедет в отдельный сервис. где под каждый тип - своя БД
			- динамическая валидация внутри примитивов, исходя из правил (к примеру: filter_element_type_operator_relation)
			- debug mode (=> items_model->toSql() )
			- response привести к массиву правил
			- при рефакторинге надо сохранить обратную совместимость
			- более насыщенное логирование ошибок для сентри. throw new Exception - явно недостаточно
			- логирование всех поисковых запросов в ELK
		*/

	}
	
	
	/**
	 * Интерфейс запуска поиска
	 *
	 * @param    array   $filters
	 * @param    null    $options
	 * @param    string  $response
	 * @return object|array
	 * @throws Exception
	 * @version        v.1.1 (03/06/2021)
	 * @author         artem
	 */
	public function search(array $filters, $options = null, string $response = 'ids')
	{
		$this->filters 		= $filters;
		$this->options 		= $options;
		$this->response 	= $response;

		// пока релизуем простой поиск
		if ($response == 'ids')
		{
			$this->items_model->select('item_id');
		}
		else
		{
			$this->items_model->select('*');
		}
		
		$this->query_builder($filters);

		$response = $this->items_model->get();
		$response = $this->modify_response($response);

		return $response;
	}
	
	
	/**
	 *
	 *
	 * @param $response
	 * @return array|mixed
	 * @author         artem
	 * @version        v.1.1
	 */
	private function modify_response($response)
	{
		if ($this->response == 'ids')
		{
			$goods = [];
			foreach ($response as $key => $item)
			{
				$goods [] = intval($item->item_id);
			}
			$response = $goods;
		}

		return $response;
	}
	
	
	/**
	 * Билдер фильтров
	 *
	 * @throws Exception
	 * @version        v.1.1 (03/06/2021)
	 * @author         artem
	 */
	private function query_builder(array $filters)
	{
		foreach ($filters as $filter)
		{
			if (!isset($filter ['object'] ['element_type_id']))
			{
				throw new Exception ('undefined element_type_id');
			}

			switch ($filter ['object'] ['element_type_id'])
			{
				case self::RELATION:
					$this->primitive_relation->query_builder($filter);
				break;

				case self::ITEM_PROPERTY:
					$this->primitive_properties->query_builder($filter);
				break;

				case self::ITEM_STATUS:
				case self::ITEM_NAME:
				case self::ITEM_TYPE:
				case self::CATEGORY:
				case self::COMPANY:
				case self::ITEM_ID:
					$this->primitive_general->query_builder($filter);
				break;
				
				default:
					throw new Exception ('unsupported element_type_id: '. $filter ['object'] ['element_type_id']);
			}
			
		}
	}
}